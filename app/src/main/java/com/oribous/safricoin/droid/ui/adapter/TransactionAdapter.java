package com.oribous.safricoin.droid.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.oribous.safricoin.droid.R;
import com.oribous.safricoin.droid.model.CryptoCurrency;
import com.oribous.safricoin.droid.model.CryptoTransaction;
import com.oribous.safricoin.droid.util.Constants;
import com.oribous.safricoin.droid.util.Lawg;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Ross Badenhorst.
 */

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {

    private Context context;
    private RecyclerViewClickListener clickListener;
    private List<CryptoTransaction> transactionList;

    public TransactionAdapter(Context context, RecyclerViewClickListener clickListener, List<CryptoTransaction> transactionList) {
        this.context = context;
        this.clickListener = clickListener;
        this.transactionList = transactionList;
    }

    @Override
    public TransactionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_transaction_item, parent, false);

        return new TransactionAdapter.ViewHolder(itemView, clickListener);
    }

    @Override
    public void onBindViewHolder(TransactionAdapter.ViewHolder holder, int position) {

        CryptoTransaction transaction = transactionList.get(position);

        //Will add a +/- in front of the zar amount to indicate a buy or sell
        String type = transaction.type == CryptoTransaction.TYPE_BUY ?
                context.getResources().getString(R.string.transaction_type_symbol_buy) :
                context.getResources().getString(R.string.transaction_type_symbol_sell);

        Lawg.i("type: " + type + ": " + transaction.cost);

        holder.zarTv.setText(type + CryptoCurrency.formatValue(context, transaction.cost, false, false));
        holder.priceTv.setText(CryptoCurrency.formatValue(context, transaction.price, false, false));
        holder.coinTv.setText(CryptoCurrency.formatValue(context, transaction.holding, false, true));

        SimpleDateFormat sdf = new SimpleDateFormat(Constants.TRANSACTION_DATE_FORMAT);
        holder.dateTv.setText(sdf.format(transaction.created));

        if (transaction.notes != null && !transaction.notes.isEmpty()) {
            holder.noteIv.setVisibility(View.VISIBLE);
        } else {
            holder.noteIv.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        public TextView zarTv;
        public TextView priceTv;
        public TextView coinTv;
        public TextView dateTv;
        public ImageView noteIv;

        private RecyclerViewClickListener clickListener;

        public ViewHolder(View itemView, RecyclerViewClickListener clickListener) {
            super(itemView);
            this.clickListener = clickListener;
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            zarTv = (TextView) itemView.findViewById(R.id.recycler_transaction_item_zar_tv);
            priceTv = (TextView) itemView.findViewById(R.id.recycler_transaction_item_price_tv);
            coinTv = (TextView) itemView.findViewById(R.id.recycler_transaction_item_coin_tv);
            dateTv = (TextView) itemView.findViewById(R.id.recycler_transaction_item_date_tv);
            noteIv = (ImageView) itemView.findViewById(R.id.recycler_transaction_item_notes_iv);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(view, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onLongClickListener(v, getAdapterPosition());
            return true;
        }
    }

    public interface RecyclerViewClickListener {

        void onItemClick(View view, int position);

        void onLongClickListener(View view, int position);
    }
}
