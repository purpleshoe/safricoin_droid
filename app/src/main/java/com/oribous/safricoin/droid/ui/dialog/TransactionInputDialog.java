package com.oribous.safricoin.droid.ui.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.oribous.safricoin.droid.R;
import com.oribous.safricoin.droid.model.CryptoCurrency;
import com.oribous.safricoin.droid.model.CryptoTransaction;
import com.oribous.safricoin.droid.network.response.GETLiveStatsResponse;
import com.oribous.safricoin.droid.util.Constants;
import com.oribous.safricoin.droid.util.Lawg;
import com.oribous.safricoin.droid.util.Notify;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Tyler Hogarth on 2017/10/16.
 */

public class TransactionInputDialog extends Dialog {

    //todo set the initial values of the input fields if they're available in the transaction obj
    /**
     * A new or existing transaction that needs to be altered and saved.
     */
    private CryptoTransaction transaction;
    /**
     * True if this is a new transaction
     */
    private boolean newTransaction;

    private OnSaveTransactionListener saveTransactionListener;

    private EditText zarEt;
    private EditText coinAmountEt;
    private EditText priceEt;
    private EditText dateEt;
    private EditText notesEt;

    private Button buyBtn;
    private Button sellBtn;
    private Button cancelBtn;
    
    public TransactionInputDialog(@NonNull Context context, CryptoTransaction transaction, boolean newTransaction) {
        super(context);
        setContentView(R.layout.dialog_transaction_input);
        this.transaction = transaction;
        this.newTransaction = newTransaction;

        initViews(context);
    }

    private void initViews(Context context) {

        zarEt = (EditText) findViewById(R.id.dialog_transaction_input_zar_et);
        if (transaction.cost > 0) {
            zarEt.setText(CryptoCurrency.formatValue(context, transaction.cost, false, false));
        }
        zarEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //we don't want the user to be able to interact with this edit text field. We set the cost automatically
                return true;
            }
        });

        coinAmountEt = (EditText) findViewById(R.id.dialog_transaction_input_coin_et);
        //watch text change to update cost value
        coinAmountEt.addTextChangedListener(getCostTextWatcher());
        coinAmountEt.setHint(transaction.symbol);
        if (transaction.holding > 0) {
            coinAmountEt.setText(CryptoCurrency.formatValue(context, transaction.holding, false, true));
        }

        priceEt = (EditText) findViewById(R.id.dialog_transaction_input_price_et);
        //watch text change to update cost value
        priceEt.addTextChangedListener(getCostTextWatcher());
        priceEt.setText(CryptoCurrency.formatValue(getContext(), transaction.price, false, true));

        dateEt = (EditText) findViewById(R.id.dialog_transaction_input_date_et);
        dateEt.setText(getDateString(new Date()));
        setDateEditText(dateEt);

        notesEt = (EditText) findViewById(R.id.dialog_transaction_input_notes_et);
        if (transaction.notes != null && !transaction.notes.isEmpty()) {
            notesEt.setText(transaction.notes);
        }

        buyBtn = (Button) findViewById(R.id.dialog_transaction_input_buy_btn);
        buyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(true);
            }
        });
        sellBtn = (Button) findViewById(R.id.dialog_transaction_input_sell_btn);
        sellBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(false);
            }
        });
        cancelBtn = (Button) findViewById(R.id.dialog_transaction_input_cancel_btn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    /**
     * We use this text watcher to detect when the price or coin amount changes so that we
     * can update the cost/zarEt value
     */
    private TextWatcher getCostTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                updateCostValue();
            }
        };
    }

    private void updateCostValue() {
        if (priceEt != null && coinAmountEt != null) {
            if (!coinAmountEt.getText().toString().isEmpty() && !priceEt.getText().toString().isEmpty()) {

                BigDecimal coin = CryptoCurrency.valueFromString(coinAmountEt.getText().toString());
                BigDecimal price = CryptoCurrency.valueFromString(priceEt.getText().toString());

                BigDecimal result = coin.multiply(price).setScale(2, BigDecimal.ROUND_HALF_UP);

                zarEt.setText(CryptoCurrency.formatValue(getContext(), result.doubleValue(), false, false));
            }
        }
    }

    /**
     * Sets the required listeners so that when the dateEt is clicked a date picker is
     * shown so the user can never type the date in manually to preserve the date format.
     * @param dateEt
     */
    private void setDateEditText(final EditText dateEt) {

        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener okListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Date date = new Date(calendar.getTimeInMillis());
                dateEt.setText(getDateString(date));
                show();
            }

        };
        final DatePickerDialog.OnCancelListener cancelListener = new DatePickerDialog.OnCancelListener(){
            @Override
            public void onCancel(DialogInterface d) {
                show();
            }
        };
        dateEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    hide();
                    DatePickerDialog datePicker = new DatePickerDialog(getContext(), okListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH));
                    datePicker.setCancelable(false);
                    datePicker.setOnCancelListener(cancelListener);
                    datePicker.show();
                }
                return true;
            }
        });
    }

    /**
     * Returns a human readable date string
     *
     * @param date
     * @return
     */
    private String getDateString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.TRANSACTION_DATE_FORMAT);
        return sdf.format(date);
    }

    /**
     * Checks to see if all required input fields have been filled in
     *
     * @return false if a field is missing
     */
    private boolean checkInputs() {

        if (zarEt.getText().length() <= 0) {

            Notify.toast(getContext(), R.string.dialog_transaction_input_err_missing_zar, Toast.LENGTH_SHORT);
            return false;
        } else if (coinAmountEt.getText().length() <= 0) {

            String message = getContext().getResources().getString(R.string.dialog_transaction_input_err_missing_coin);
            message = message.replace("{:value}", transaction.symbol);
            Notify.toast(getContext(), message, Toast.LENGTH_SHORT);
            return false;
        } else if (priceEt.getText().length() <= 0) {

            Notify.toast(getContext(), R.string.dialog_transaction_input_err_missing_price, Toast.LENGTH_SHORT);
            return false;
        } else if (dateEt.getText().length() <= 0) {

            Notify.toast(getContext(), R.string.dialog_transaction_input_err_missing_date, Toast.LENGTH_SHORT);
            return false;
        }

        return true;
    }

    /**
     * Uses the input data to create and save the transaction
     *
     * @param buy true if the user wants to set this transaction as a buy. False if sell
     */
    private void save(boolean buy) {
        Log.i(CryptoCurrency.TAG, "save");
        if (checkInputs()) {
            try {

                transaction.type = buy ? CryptoTransaction.TYPE_BUY : CryptoTransaction.TYPE_SELL;
                Lawg.i("type: " + transaction.type);
                transaction.cost = CryptoCurrency.valueFromString(zarEt.getText().toString()).doubleValue();
                String holding = buy ? "" + coinAmountEt.getText().toString() : "-" + coinAmountEt.getText().toString();

                transaction.holding = CryptoCurrency.valueFromString(coinAmountEt.getText().toString()).doubleValue();
                SimpleDateFormat sdf = new SimpleDateFormat(Constants.TRANSACTION_DATE_FORMAT);
                transaction.created = sdf.parse(dateEt.getText().toString());;
                transaction.modified = new Date();
                transaction.price = CryptoCurrency.valueFromString(priceEt.getText().toString()).doubleValue();
                transaction.notes = notesEt.getText().toString();

                CryptoTransaction.save(transaction);

                if (saveTransactionListener != null) {
                    saveTransactionListener.onSaveTransaction(transaction, newTransaction);
                }

                dismiss();
            } catch (Exception e) {
                e.printStackTrace();
                Notify.toast(getContext(), R.string.dialog_transaction_input_err_saving, Toast.LENGTH_SHORT);
            }
        }
    }

    public void setOnSaveTransactionListener(OnSaveTransactionListener cb) {
        saveTransactionListener = cb;
    }

    public interface OnSaveTransactionListener {
        void onSaveTransaction(CryptoTransaction transaction, boolean newTransaction);
    }
}
