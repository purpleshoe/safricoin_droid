package com.oribous.safricoin.droid;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Ross Badenhorst.
 */

public class App extends com.orm.SugarApp {

    private static RequestQueue requestQueue;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    /**
     * Gets the request queue for volley.
     * @return
     */
    public static RequestQueue getRequestQueue(Context context) {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }
        return requestQueue;
    }
}
