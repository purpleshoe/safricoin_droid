package com.oribous.safricoin.droid.network.request;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.oribous.safricoin.droid.network.response.BaseResponse;
import com.oribous.safricoin.droid.util.Lawg;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ross Badenhorst.
 */

public abstract class BaseRequest<T extends BaseResponse> extends Request<T> {
    public static final String TAG = BaseRequest.class.getSimpleName();

    public static final String ROUTE_BASE       = "https://www.altcointrader.co.za/api/v2";
    public static final String ROUTE_LIVE_STATS = "/live-stats";

    public static final int   RETRY_TIMEOUT   = 30000;
    public static final int   RETRY_COUNT     = 0;
    public static final float RETRY_BACKOFF   = 1f;

    protected static final Gson gson = new Gson();
    private final Class<T> clazz;
    private final Response.Listener<T> listener;
    protected final Context context;

    /**
     * The headers that are part of the HTTP request.
     */
    protected Map<String, String> headers;
    /**
     * The params that are part of the HTTP request.
     */
    protected Map<String, String> params;

    public BaseRequest(Context context, int method, String url, Class<T> clazz, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.context    = context;
        this.clazz      = clazz;
        this.listener   = listener;

        Lawg.i(clazz.getSimpleName() + " - URL: " + url);

        setRetryPolicy(new DefaultRetryPolicy(RETRY_TIMEOUT,
                RETRY_COUNT,
                RETRY_BACKOFF));
    }

    public static String getRoute(String route) {
        return  ROUTE_BASE + route;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        super.getHeaders();
        headers = new HashMap<>();

        return headers;
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        super.getParams();
        params = new HashMap<>();
        return params;
    }

    /**
     * Sometimes we need to do certain actions every single time a request is fired.
     * A good example of this is the users balance.
     * If we do a request to display the balance or just to update it, on both occasions we need to persist it.
     * Logic like that belongs in onPreResponse.
     *
     * This is called just before the listener receives the call back of the network response.
     */
    protected void onPreResponse(T response) {
    }

    @Override
    protected void deliverResponse(T response) {
        onPreResponse(response);
        if (listener != null) {
            listener.onResponse(response);
        }
    }

    protected void onPreError(VolleyError error) {
    }

    @Override
    public void deliverError(VolleyError error) {
        printError(error);
        onPreError(error);
        super.deliverError(error);
    }

    protected void printError(VolleyError error) {
        if (error != null && error.getMessage() != null) {
            Lawg.e(clazz.getSimpleName() + " ERROR: " + error.getMessage());
        } else {
            Lawg.e(clazz.getSimpleName() + " ERROR: Request Failed!");
        }
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Lawg.i(clazz.getSimpleName() + " JSON: " + json);
            return Response.success(gson.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }
}
