package com.oribous.safricoin.droid.worker.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.oribous.safricoin.droid.util.Constants;

/**
 * Created by Ross Badenhorst.
 */

public class BootReceiver extends BroadcastReceiver {
    public static final String TAG = BootReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if ((action != null) && (action.equals(Intent.ACTION_BOOT_COMPLETED))) {
            Intent priceUpdaterReceiverIntent = new Intent(context, PriceUpdaterReceiver.class);
            PendingIntent alarmBalIntent = PendingIntent.getBroadcast(context, 0, priceUpdaterReceiverIntent, 0);
            AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, Constants.PRICE_CHECK_INTERVAL, Constants.PRICE_CHECK_INTERVAL, alarmBalIntent);
        }
    }
}
