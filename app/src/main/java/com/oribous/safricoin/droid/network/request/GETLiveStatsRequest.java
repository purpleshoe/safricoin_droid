package com.oribous.safricoin.droid.network.request;

import android.content.Context;

import com.android.volley.Response;
import com.oribous.safricoin.droid.model.CryptoCurrency;
import com.oribous.safricoin.droid.network.response.GETLiveStatsResponse;
import com.oribous.safricoin.droid.util.Constants;
import com.oribous.safricoin.droid.util.Prefs;

import java.util.Date;

/**
 * Created by Ross Badenhorst.
 */

public class GETLiveStatsRequest extends BaseRequest<GETLiveStatsResponse> {
    public static final String TAG = GETLiveStatsRequest.class.getSimpleName();

    public GETLiveStatsRequest(Context context, Response.Listener<GETLiveStatsResponse> listener, Response.ErrorListener errorListener) {
        super(context, Method.GET, getEndpoint(context), GETLiveStatsResponse.class, listener, errorListener);
    }

    private static String getEndpoint(Context context) {
        return getRoute(ROUTE_LIVE_STATS);
    }

    @Override
    protected void onPreResponse(GETLiveStatsResponse response) {
        super.onPreResponse(response);

        CryptoCurrency.deleteAll(CryptoCurrency.class);

        new CryptoCurrency(CryptoCurrency.SYMBOL_BITCOIN, response.BTC.Price).save();
        new CryptoCurrency(CryptoCurrency.SYMBOL_BITCOIN_CASH, response.BCC.Price).save();
        new CryptoCurrency(CryptoCurrency.SYMBOL_LITECOIN, response.LTC.Price).save();
        new CryptoCurrency(CryptoCurrency.SYMBOL_NAMECOIN, response.NMC.Price).save();
        new CryptoCurrency(CryptoCurrency.SYMBOL_RIPPLE, response.XRP.Price).save();
        new CryptoCurrency(CryptoCurrency.SYMBOL_ETHERIUM, response.ETH.Price).save();
        new CryptoCurrency(CryptoCurrency.SYMBOL_DASH, response.DASH.Price).save();
        new CryptoCurrency(CryptoCurrency.SYMBOL_ZCASH, response.ZEC.Price).save();

        // Only check for new prices an hour in the future (stops thrashing the server)
        Prefs.put(context, Prefs.NEXT_PRICE_UPDATE, new Date().getTime() + Constants.RATE_LIMT * 1000);
    }
}
