package com.oribous.safricoin.droid.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.oribous.safricoin.droid.R;
import com.oribous.safricoin.droid.model.CryptoCurrency;

import java.util.List;

import static com.oribous.safricoin.droid.R.id.recycler_crypto_item_profit_loss_tv;

/**
 * Created by Ross Badenhorst.
 */
public class CryptoCurrencyAdapter extends RecyclerView.Adapter<CryptoCurrencyAdapter.ViewHolder> {

    private Context context;
    private RecyclerViewClickListener clickListener;
    private List<CryptoCurrency> cryptoCurrencyList;

    public CryptoCurrencyAdapter(Context context, RecyclerViewClickListener clickListener, List<CryptoCurrency> cryptoCurrencyList) {
        this.context = context;
        this.clickListener = clickListener;
        this.cryptoCurrencyList = cryptoCurrencyList;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_crypto_item, parent, false);
        return new ViewHolder(itemView, clickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CryptoCurrency cryptoCurrency = cryptoCurrencyList.get(position);

        double takings = cryptoCurrency.marketValue.doubleValue();

        holder.iconIv.setImageDrawable(context.getResources().getDrawable(cryptoCurrency.getDrawable()));
        holder.symbolTv.setText(cryptoCurrency.symbol);
        holder.amountTv.setText(CryptoCurrency.formatValue(context, CryptoCurrency.getHoldingsPerCryptoCurrency(cryptoCurrency), true, true));
        holder.profitLossTv.setText(CryptoCurrency.formatValue(context, takings, true, false));
        holder.priceTv.setText(CryptoCurrency.formatValue(context, CryptoCurrency.getDoublePrice(cryptoCurrency.price), true, false));

        if (takings > 0) {
            holder.profitLossTv.setTextColor(context.getResources().getColor(R.color.value_up));
        } else if (takings < 0) {
            holder.profitLossTv.setTextColor(context.getResources().getColor(R.color.value_down));
        } else {
            holder.profitLossTv.setTextColor(context.getResources().getColor(R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        return cryptoCurrencyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RecyclerViewClickListener clickListener;

        public ImageView iconIv;
        public TextView symbolTv;
        public TextView amountTv;
        public TextView profitLossTv;
        public TextView priceTv;



        public ViewHolder(View itemView, RecyclerViewClickListener clickListener) {
            super(itemView);
            this.clickListener = clickListener;
            itemView.setOnClickListener(this);

            iconIv = (ImageView) itemView.findViewById(R.id.recycler_crypto_item_iv);
            symbolTv = (TextView) itemView.findViewById(R.id.recycler_crypto_item_symbol_tv);
            amountTv = (TextView) itemView.findViewById(R.id.recycler_crypto_item_amount_tv);
            profitLossTv = (TextView) itemView.findViewById(recycler_crypto_item_profit_loss_tv);
            priceTv = (TextView) itemView.findViewById(R.id.recycler_crypto_item_price_tv);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public interface RecyclerViewClickListener {

        void onItemClick(View view, int position);
    }
}

