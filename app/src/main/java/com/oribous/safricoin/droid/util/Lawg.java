package com.oribous.safricoin.droid.util;

import android.util.Log;

/**
 * Created by Ross Badenhorst.
 */

public class Lawg {

    public static boolean DATA_CONTROL_LOGS_ON = false;

    static String className;
    static String methodName;
    static int lineNumber;

    public static String createLog(String log) {

        StringBuffer buffer = new StringBuffer();
        buffer.append("[");
        buffer.append(methodName);
        buffer.append(":");
        buffer.append(lineNumber);
        buffer.append("]");
        buffer.append(log);

        return buffer.toString();
    }

    public static void getMethodNames(StackTraceElement[] sElements) {
        className = sElements[1].getFileName().replace(".java", "");
        methodName = sElements[1].getMethodName() + "()";
        lineNumber = sElements[1].getLineNumber();
    }

    public static void e(String msg) {
        getMethodNames(new Throwable().getStackTrace());
        Log.e(className, createLog(msg));
    }

    public static void w(String msg) {
        getMethodNames(new Throwable().getStackTrace());
        Log.w(className, createLog(msg));
    }

    public static void v(String msg) {
        getMethodNames(new Throwable().getStackTrace());
        Log.v(className, createLog(msg));
    }

    public static void i(String msg) {
        getMethodNames(new Throwable().getStackTrace());
        Log.i(className, createLog(msg));
    }

    public static void i(Object obj) {
        getMethodNames(new Throwable().getStackTrace());
        Log.i(className, createLog(obj.toString()));
    }

    public static void i() {
        getMethodNames(new Throwable().getStackTrace());
        Log.i(className, createLog(""));
    }

    public static void d(String msg) {
        getMethodNames(new Throwable().getStackTrace());
        Log.d(className, createLog(msg));
    }

    public static void e(String tag, String msg) {
        getMethodNames(new Throwable().getStackTrace());
        Log.e(tag, createLog(msg));
    }

    public static void w(String tag, String msg) {
        getMethodNames(new Throwable().getStackTrace());
        Log.w(tag, createLog(msg));
    }

    public static void i(String tag, String msg) {
        getMethodNames(new Throwable().getStackTrace());
        Log.i(tag, createLog(msg));
    }

    public static void d(String tag, String msg) {
        getMethodNames(new Throwable().getStackTrace());
        Log.d(tag, createLog(msg));
    }
}
