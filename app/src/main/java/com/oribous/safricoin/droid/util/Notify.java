package com.oribous.safricoin.droid.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.Toast;

import com.oribous.safricoin.droid.R;

/**
 * Created by Tyler on 2015-11-05.
 */
public class Notify {

    public static void toast(Context context, int resId, int duration) {

        toast(context, context.getResources().getString(resId), duration);
    }

    public static void toast(Context context, String msg, int duration) {

        Toast toast = Toast.makeText(context, msg, duration);
        toast.setGravity(Gravity.TOP, 0, 200);
        toast.show();
    }

    public static void toastBottom(Context context, int resId, int duration) {

        toastBottom(context, context.getResources().getString(resId), duration);
    }

    public static void toastBottom(Context context, String msg, int duration) {

        Toast toast = Toast.makeText(context, msg, duration);
        toast.setGravity(Gravity.BOTTOM, 0, 200);
        toast.show();
    }

    public static AlertDialog alertDialog(Context context, String title, int msgId) {

        return alertDialog(context, title, context.getResources().getString(msgId), null);
    }

    public static AlertDialog alertDialog(Context context, String title, String msg) {

        return alertDialog(context, title, msg, null);
    }

    public static AlertDialog alertDialog(Context context, String title, String msg,
                                          DialogInterface.OnClickListener okListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.dialog_style);
        builder.setMessage(msg);
        if (!title.isEmpty()) {
            builder.setTitle(title);
        }
        builder.setPositiveButton(R.string.dialog_ok, okListener);

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
        return dialog;
    }

    public static AlertDialog yesCancelDialog(Context context, String title, int strId,
                                       DialogInterface.OnClickListener yes, DialogInterface.OnClickListener cancel) {

        return yesCancelDialog(context, title, context.getResources().getString(strId), yes, cancel);
    }

    public static AlertDialog yesCancelDialog(Context context, String title, String str,
                               DialogInterface.OnClickListener yes, DialogInterface.OnClickListener cancel) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.dialog_style);
        builder.setMessage(str);
        if (!title.isEmpty()) {
            builder.setTitle(title);
        }
        builder.setPositiveButton(R.string.dialog_yes, yes);

        if (cancel == null) {
            cancel = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };
        }
        builder.setNegativeButton(R.string.dialog_cancel, cancel);

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
        return dialog;
    }

    public static void yesNoDialog(Context context, int titleId, int strId,
                                   DialogInterface.OnClickListener yes, DialogInterface.OnClickListener no) {

        yesNoDialog(context, context.getResources().getString(titleId), context.getResources().getString(strId), yes, no);
    }

    public static void yesNoDialog(Context context, String title, int strId,
                                       DialogInterface.OnClickListener yes, DialogInterface.OnClickListener no) {

        yesNoDialog(context, title, context.getResources().getString(strId), yes, no);
    }

    public static void yesNoDialog(Context context, String title, String str,
                                       DialogInterface.OnClickListener yes, DialogInterface.OnClickListener no) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.dialog_style);
        builder.setMessage(str);
        if (!title.isEmpty()) {
            builder.setTitle(title);
        }
        builder.setPositiveButton(R.string.dialog_yes, yes);

        if (no == null) {
            no = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };
        }
        builder.setNegativeButton(R.string.dialog_no, no);

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        dialog.show();
    }

    public static Dialog yesNoCustomButtonDialog(Context context, String title, String str,
                                               String yesBtn, String noBtn,
                                   DialogInterface.OnClickListener yes, DialogInterface.OnClickListener no) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.dialog_style);
        builder.setMessage(str);
        if (!title.isEmpty()) {
            builder.setTitle(title);
        }
        builder.setPositiveButton(yesBtn, yes);

        if (no == null) {
            no = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };
        }
        builder.setNegativeButton(noBtn, no);

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        dialog.show();

        return dialog;
    }

    public static ProgressDialog progressDialog(Context context, String title, int strRes, boolean cancelable) {
        return progressDialog(context, title, context.getResources().getString(strRes), cancelable);
    }

    public static ProgressDialog progressDialog(Context context, String title, String str, boolean cancelable) {
        ProgressDialog progressDialog = new ProgressDialog(context, R.style.dialog_style);
        progressDialog.setTitle(title);
        progressDialog.setMessage(str);
        progressDialog.setCancelable(cancelable);
        progressDialog.show();
        return progressDialog;
    }

    public static ProgressDialog progressDialogBackFinisher(final Activity activity, String title, String message) {
        ProgressDialog progressDialog = new ProgressDialog(activity, R.style.dialog_style);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);

        //we want the user to be able to get out of this screen
        //so this lets us control how the dialog is canceled
        progressDialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    activity.finish();
                }
                return true;
            }
        });

        progressDialog.show();
        return progressDialog;
    }

    public static ProgressDialog progressDialogBackDismiss(final Context context, String title, String str) {
        ProgressDialog progressDialog = new ProgressDialog(context, R.style.dialog_style);
        progressDialog.setTitle(title);
        progressDialog.setMessage(str);
        progressDialog.setCancelable(false);

        //we want the user to be able to get out of this screen
        //so this lets us control how the dialog is canceled
        progressDialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                }
                return true;
            }
        });

        progressDialog.show();
        return progressDialog;
    }
}