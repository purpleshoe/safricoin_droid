package com.oribous.safricoin.droid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

import java.util.Comparator;
import java.util.Date;

/**
 * Created by Ross Badenhorst.
 */
public class CryptoTransaction extends SugarRecord implements Parcelable {

    public static final String TAG = CryptoTransaction.class.getSimpleName();

    public static final int TYPE_BUY = 0;
    public static final int TYPE_SELL = 1;

    /**
     * Date the transaction was created.
     */
    public Date created;
    /**
     * Date the transcation was last modified.
     */
    public Date modified;
    /**
     * Either TYPE_BUY or TYPE_SELL
     */
    public int type = TYPE_BUY;
    /**
     * The amount of crypto the transaction contains
     * e.g. 13.5343 BTC
     */
    public double holding;
    /**
     * The total cost of the holding.
     * e.g holding * price
     * e.g 13.5343 * R12 = R162.41
     */
    public double cost;
    /**
     * The market value of the crypto at the time of purchase.
     * e.g R12
     */
    public double price;
    /**
     * The symbol of the crypto for the transaction
     * e.g BTC
     */
    public String symbol;
    /**
     * Any notes the user set for this transaction.
     */
    public String notes;

    public CryptoTransaction() {

    }

    public CryptoTransaction(String symbol, double holding, double price) {
        this.symbol = symbol;
        this.holding = holding;
        this.price = price;

        created = new Date();
        modified = new Date();
    }

    public static Comparator<CryptoTransaction> getComparator() {
        return new Comparator<CryptoTransaction>() {
            @Override
            public int compare(CryptoTransaction lhs, CryptoTransaction rhs) {
                return rhs.created.compareTo(lhs.created);
            }
        };
    }

    protected CryptoTransaction(Parcel in) {
        type = in.readInt();
        long tmpCreated = in.readLong();
        created = tmpCreated != -1 ? new Date(tmpCreated) : null;
        long tmpModified = in.readLong();
        modified = tmpModified != -1 ? new Date(tmpModified) : null;
        holding = in.readDouble();
        cost = in.readDouble();
        price = in.readDouble();
        symbol = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(type);
        dest.writeLong(created != null ? created.getTime() : -1L);
        dest.writeLong(modified != null ? modified.getTime() : -1L);
        dest.writeDouble(holding);
        dest.writeDouble(cost);
        dest.writeDouble(price);
        dest.writeString(symbol);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CryptoTransaction> CREATOR = new Parcelable.Creator<CryptoTransaction>() {
        @Override
        public CryptoTransaction createFromParcel(Parcel in) {
            return new CryptoTransaction(in);
        }

        @Override
        public CryptoTransaction[] newArray(int size) {
            return new CryptoTransaction[size];
        }
    };
}