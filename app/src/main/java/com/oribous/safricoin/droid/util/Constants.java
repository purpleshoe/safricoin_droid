package com.oribous.safricoin.droid.util;

/**
 * Created by Ross Badenhorst.
 */

public class Constants {

    /**
     * How often the device should check for updated prices.
     */
    public static final int PRICE_CHECK_INTERVAL = 3600;
    /**
     * The most the device can do a live-stats check.
     * The intent service will be scheduled on PRICE_CHECK_INTERVAL.
     * But things like pull to refresh and the activities will limit themselves on RATE_LIMT.
     */
    public static final int RATE_LIMT = 60;

    public static final String TRANSACTION_DATE_FORMAT = "dd-MMM-yy";

    /**
     * All whole numbers, rands or crypto, shouldn't have any decimal places
     */
    public static final String NUMBER_FORMAT_NO_DECIMALS = "#,##0";
    /**
     * Numbers such as Rand or fiat is typically displayed to two decimal places
     */
    public static final String NUMBER_FORMAT_DECIMALS_TWO = "#,##0.00";
    /**
     * Cryptos should allow for up to 8 decimal places.
     */
    public static final String NUMBER_FORMAT_DECIMALS_EIGHT = "#,##0.########";

    public static final char NUMBER_FORMAT_THOUSANDS_SEPERATOR = ' ';
}
