package com.oribous.safricoin.droid.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.oribous.safricoin.droid.App;
import com.oribous.safricoin.droid.R;
import com.oribous.safricoin.droid.model.CryptoCurrency;
import com.oribous.safricoin.droid.network.request.GETLiveStatsRequest;
import com.oribous.safricoin.droid.network.response.GETLiveStatsResponse;
import com.oribous.safricoin.droid.ui.adapter.CryptoCurrencyAdapter;
import com.oribous.safricoin.droid.util.DeviceUtil;
import com.oribous.safricoin.droid.util.Prefs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Ross Badenhorst.
 */
public class MainActivity extends Activity implements CryptoCurrencyAdapter.RecyclerViewClickListener {
    public static final String TAG = MainActivity.class.getSimpleName();

    private SwipeRefreshLayout swipeLayout;
    private TextView profitLossTv;
    private RecyclerView recycler;

    private List<CryptoCurrency> cryptoCurrencyList = new ArrayList<>();
    private CryptoCurrencyAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_lv_sl);
        recycler = (RecyclerView) findViewById(R.id.activity_main_coins_lv);
        profitLossTv = (TextView) findViewById(R.id.activity_main_profit_loss_tv);

        adapter = new CryptoCurrencyAdapter(this, this, cryptoCurrencyList);

        recycler.setAdapter(adapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(mLayoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updatePrices();
            }
        });
        swipeLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    swipeLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    swipeLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                if (DeviceUtil.isInternetConnection(swipeLayout.getContext())) {
                    swipeLayout.setRefreshing(true);
                    updatePrices();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        onPricesUpdated(CryptoCurrency.listAll(CryptoCurrency.class));

        if (new Date().getTime() > Prefs.getLong(this, Prefs.NEXT_PRICE_UPDATE)) {
            updatePrices();
        }
    }

    /**
     * Called when the prices of the crypto currencies have been updated
     * @param cryptoCurrencyList - list of new prices
     */
    private void onPricesUpdated(List<CryptoCurrency> cryptoCurrencyList) {
        for (int i =0 ; i < cryptoCurrencyList.size(); i++) {
            cryptoCurrencyList.get(i).marketValue = new BigDecimal(CryptoCurrency.getTakingsPerCryptoCurrency(cryptoCurrencyList.get(i)));
        }
        Collections.sort(cryptoCurrencyList);


        this.cryptoCurrencyList.clear();
        this.cryptoCurrencyList.addAll(cryptoCurrencyList);

        adapter.notifyDataSetChanged();
        profitLossTv.setText(CryptoCurrency.formatValue(this, CryptoCurrency.getTotalTakings(), false, false));
        swipeLayout.setRefreshing(false);
    }

    /**
     * Fires a network request to update the prices of the crypto currencies.
     */
    private void updatePrices() {
        if (System.currentTimeMillis() > Prefs.getLong(this, Prefs.NEXT_PRICE_UPDATE)) {
            GETLiveStatsRequest request = new GETLiveStatsRequest(this, new Response.Listener<GETLiveStatsResponse>() {
                @Override
                public void onResponse(GETLiveStatsResponse response) {
                    onPricesUpdated(CryptoCurrency.listAll(CryptoCurrency.class));
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            App.getRequestQueue(this).add(request);
        } else {
            swipeLayout.setRefreshing(false);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent transactionsActivityIntent = new Intent(this, TransactionsActivity.class);
        transactionsActivityIntent.putExtra(TransactionsActivity.KEY_CRYPTO_CURRENCY, cryptoCurrencyList.get(position));
        startActivity(transactionsActivityIntent);
    }
}
