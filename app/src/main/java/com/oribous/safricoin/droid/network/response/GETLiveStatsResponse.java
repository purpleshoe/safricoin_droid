package com.oribous.safricoin.droid.network.response;

/**
 * Created by Ross Badenhorst.
 */

public class GETLiveStatsResponse extends BaseResponse {
    public static final String TAG = GETLiveStatsResponse.class.getSimpleName();

    public Crypto BTC;
    public Crypto BCC;
    public Crypto LTC;
    public Crypto NMC;
    public Crypto XRP;
    public Crypto ETH;
    public Crypto DASH;
    public Crypto ZEC;

    public class Crypto {
        public String Price;
    }
}
