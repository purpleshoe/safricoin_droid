package com.oribous.safricoin.droid.worker.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.oribous.safricoin.droid.App;
import com.oribous.safricoin.droid.network.request.GETLiveStatsRequest;
import com.oribous.safricoin.droid.network.response.GETLiveStatsResponse;
import com.oribous.safricoin.droid.util.Prefs;

import java.util.Date;

/**
 * Created by Ross Badenhorst.
 */

public class PriceUpdaterReceiver extends BroadcastReceiver implements Response.Listener<GETLiveStatsResponse>, Response.ErrorListener {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (new Date().getTime() > Prefs.getLong(context, Prefs.NEXT_PRICE_UPDATE)) {
            GETLiveStatsRequest request = new GETLiveStatsRequest(context, this, this);
            App.getRequestQueue(context).add(request);
        }
    }

    @Override
    public void onResponse(GETLiveStatsResponse response) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
