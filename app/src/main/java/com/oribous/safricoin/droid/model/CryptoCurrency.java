package com.oribous.safricoin.droid.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;

import com.oribous.safricoin.droid.R;
import com.oribous.safricoin.droid.util.Constants;
import com.orm.SugarRecord;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static com.oribous.safricoin.droid.R.drawable.ic_crypto_default;

/**
 * Created by Ross Badenhorst.
 */
public class CryptoCurrency extends SugarRecord implements Parcelable, Comparable<CryptoCurrency> {
    public static final String TAG = CryptoCurrency.class.getSimpleName();

    /**
     * Date the CryptoCurrency was created.
     */
    public Date created;
    /**
     * Date the CryptoCurrency was last modified.
     */
    public Date modified;
    /**
     * The symbol of the crypto. Typically a three letter acronym.
     * e.g BTC
     */
    public String symbol;
    /**
     * The currency market value of the crypto (at the time of modified)
     * Kept as a string to preserve precision
     * e.g R12
     */
    public String price;
    /**
     * How much profit or loss the user has made on the cryptocurrency.
     * We calculate this based off the CryptoTransactions with the same symbol.
     */
    public BigDecimal marketValue;

    public static final String SYMBOL_BITCOIN       = "BTC";
    public static final String SYMBOL_BITCOIN_CASH  = "BCC";
    public static final String SYMBOL_LITECOIN      = "LTC";
    public static final String SYMBOL_NAMECOIN      = "NMC";
    public static final String SYMBOL_RIPPLE        = "XRP";
    public static final String SYMBOL_ETHERIUM      = "ETH";
    public static final String SYMBOL_DASH          = "DASH";
    public static final String SYMBOL_ZCASH         = "ZEC";

    public CryptoCurrency() {
        created = new Date();
        modified = new Date();
    }

    public CryptoCurrency(String symbol, String price) {
        created = new Date();
        modified = new Date();
        this.symbol = symbol;
        this.price = price;
    }

    /**
     * Formats values to be displayed.
     *
     * If we have decimal places, we always want to display two:
     * Examples:
     * 12.1 becomes 12.10
     * 14.24 becomes 14.24
     * 390.235 becomes 390.235
     *
     * If there are no decimal places, we never want to display any.
     * Examples:
     * 12 stays 12
     *
     * If the number is large, we want to seperate thousands by a space (With the rules applied above)
     * Examples:
     * 12345 becomes 12 345
     *
     * @param context - of the application.
     * @param value - to be formatted.
     * @param returnDashOnZero - if we should return the zero or a dash in is place.
     * @return Something that is nice to look at.
     */
    public static String formatValue(Context context, double value, boolean returnDashOnZero, boolean displayMoreThenTwoDecimals) {
        if(returnDashOnZero && value == 0) {
            return context.getResources().getString(R.string.no_currency_value);
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator(Constants.NUMBER_FORMAT_THOUSANDS_SEPERATOR);

        if (isWholeNumber(value)) {
            DecimalFormat format = new DecimalFormat(Constants.NUMBER_FORMAT_NO_DECIMALS, symbols);
            return format.format(value);
        } else if (displayMoreThenTwoDecimals)  {
            DecimalFormat format = new DecimalFormat(Constants.NUMBER_FORMAT_DECIMALS_EIGHT, symbols);
            return format.format(value);
        } else {
            DecimalFormat format = new DecimalFormat(Constants.NUMBER_FORMAT_DECIMALS_TWO, symbols);
            return format.format(value);
        }
    }

    /**
     * Takes a string that is formated with spaces and converts it to a BigDecimal.
     * @param value - formated String: 78 000.02
     * @return BigDecimal value of the string.
     */
    public static BigDecimal valueFromString(String value) {
        value = value.replace(" ", "");
        return new BigDecimal(value);
    }

    /**
     * Checks if a double has a decimal place or is a whole number.
     * @param value - to check if there is a decimal place.
     * @return true if yes, false if now.
     */
    private static boolean isWholeNumber(double value) {
        if (value % 1 == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * We keep price in string (to preserve precision), this is used to convert the price to a double.
     * @param price - in string format
     * @return price in double format (or 0 if it cant be converted)
     */
    public static double getDoublePrice(String price) {
        try {
            return Double.parseDouble(price);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Calculates the total takings (profit or loss) for all CryptoCurrencies.
     * @return - total takings for all cyrptos
     */
    public static double getTotalTakings() {
        List<CryptoCurrency> cryptoCurrencies = CryptoCurrency.listAll(CryptoCurrency.class);

        double totalTakings = 0;
        for (CryptoCurrency cryptoCurrency : cryptoCurrencies) {
            totalTakings += getTakingsPerCryptoCurrency(cryptoCurrency);
        }

        return totalTakings;
    }

    public static double getHoldingsPerCryptoCurrency(CryptoCurrency cryptoCurrency) {
        List<CryptoTransaction> transactions = CryptoTransaction.find(CryptoTransaction.class, "symbol = ?", cryptoCurrency.symbol);

        double holdings = 0;
        for (CryptoTransaction transaction : transactions) {
            holdings += transaction.holding;
        }

        return holdings;
    }

    public static double getCostPerCryptoCurrency(CryptoCurrency cryptoCurrency) {
        List<CryptoTransaction> transactions = CryptoTransaction.find(CryptoTransaction.class, "symbol = ?", cryptoCurrency.symbol);

        double cost = 0;
        for (CryptoTransaction transaction : transactions) {
            cost += transaction.cost;
        }

        return cost;
    }

    /**
     * Calculates the takings for a certain Crypto.
     * @param cryptoCurrency - to calculates takings for
     * @return total profit/loss (takings) for cryptoCurrency
     */
    public static double getTakingsPerCryptoCurrency(CryptoCurrency cryptoCurrency) {
        double holdings = getHoldingsPerCryptoCurrency(cryptoCurrency);
        double cost = getCostPerCryptoCurrency(cryptoCurrency);

        return (holdings * getDoublePrice(cryptoCurrency.price)) - cost;
    }

    /**
     * Gets the icon for the crypto based on its symbol.
     *
     * @return - drawable id matching the crypto's icon
     */
    public int getDrawable() {
        return getDrawable(this.symbol);
    }

    /**
     * Gets the icon fo a crypto based on the symbol parameter.
     * Used where you want to get an icon for a crytpo using a transaction model.
     *
     * @param symbol - of the crypto
     * @return - drawable id matching the crypto's icon
     */
    public int getDrawable(String symbol) {

        if (symbol.equalsIgnoreCase(SYMBOL_BITCOIN)) {
            return R.drawable.ic_crypto_btc;
        }

        if (symbol.equalsIgnoreCase(SYMBOL_BITCOIN_CASH)) {
            return R.drawable.ic_crypto_bcc;
        }

        if (symbol.equalsIgnoreCase(SYMBOL_LITECOIN)) {
            return R.drawable.ic_crypto_ltc;
        }

        if (symbol.equalsIgnoreCase(SYMBOL_NAMECOIN)) {
            return R.drawable.ic_crypto_nmc;
        }

        if (symbol.equalsIgnoreCase(SYMBOL_RIPPLE)) {
            return R.drawable.ic_crypto_xrp;
        }

        if (symbol.equalsIgnoreCase(SYMBOL_ETHERIUM)) {
            return R.drawable.ic_crypto_eth;
        }

        if (symbol.equalsIgnoreCase(SYMBOL_DASH)) {
            return R.drawable.ic_crypto_dash;
        }

        if (symbol.equalsIgnoreCase(SYMBOL_ZCASH)) {
            return R.drawable.ic_crypto_zec;
        }

        return ic_crypto_default;
    }

    protected CryptoCurrency(Parcel in) {
        long tmpCreated = in.readLong();
        created = tmpCreated != -1 ? new Date(tmpCreated) : null;
        long tmpModified = in.readLong();
        modified = tmpModified != -1 ? new Date(tmpModified) : null;
        symbol = in.readString();
        price = in.readString();
    }

    @Override
    public int compareTo(@NonNull CryptoCurrency cryptoCurrency) {
        BigDecimal a;
        BigDecimal b;

        if (cryptoCurrency.marketValue.intValue() == 0 ) {
            a = new BigDecimal(-999999999);
        } else {
            a = cryptoCurrency.marketValue;
        }

        if (this.marketValue.intValue() == 0) {
            b = new BigDecimal(-999999999);
        } else {
            b = this.marketValue;
        }

        return a.intValue() - b.intValue();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(created != null ? created.getTime() : -1L);
        dest.writeLong(modified != null ? modified.getTime() : -1L);
        dest.writeString(symbol);
        dest.writeString(price);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CryptoCurrency> CREATOR = new Parcelable.Creator<CryptoCurrency>() {
        @Override
        public CryptoCurrency createFromParcel(Parcel in) {
            return new CryptoCurrency(in);
        }

        @Override
        public CryptoCurrency[] newArray(int size) {
            return new CryptoCurrency[size];
        }
    };
}
