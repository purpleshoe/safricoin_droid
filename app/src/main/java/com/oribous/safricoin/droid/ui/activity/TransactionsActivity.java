package com.oribous.safricoin.droid.ui.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.oribous.safricoin.droid.R;
import com.oribous.safricoin.droid.model.CryptoCurrency;
import com.oribous.safricoin.droid.model.CryptoTransaction;
import com.oribous.safricoin.droid.ui.adapter.TransactionAdapter;
import com.oribous.safricoin.droid.ui.dialog.TransactionInputDialog;
import com.oribous.safricoin.droid.util.Notify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Ross Badenhorst.
 */
public class TransactionsActivity extends AppCompatActivity implements TransactionAdapter.RecyclerViewClickListener {
    public static final String TAG = TransactionsActivity.class.getSimpleName();

    /**
     * Passed from calling activites.
     * We use this to display all transactions for a specific crypto
     */
    public static final String KEY_CRYPTO_CURRENCY = "KEY_CRYPTO_CURRENCY";

    private List<CryptoTransaction> transactionList = new ArrayList<>();;
    private CryptoCurrency cryptoCurrency = new CryptoCurrency(CryptoCurrency.SYMBOL_BITCOIN, "0");

    private RecyclerView recycler;
    private TransactionAdapter adapter;

    private FloatingActionButton fab;
    private TextView coinLabelTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        coinLabelTv = (TextView) findViewById(R.id.activity_transactions_coin_label_tv);
        fab = (FloatingActionButton) findViewById(R.id.activity_transactions_fab);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        recycler = (RecyclerView) findViewById(R.id.activity_transactions_lv);
        adapter = new TransactionAdapter(this, this, transactionList);

        recycler.setAdapter(adapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(mLayoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInputDialog(new CryptoTransaction(cryptoCurrency.symbol, 0, CryptoCurrency.getDoublePrice(cryptoCurrency.price)), true);
            }
        });

        if (getIntent().hasExtra(KEY_CRYPTO_CURRENCY)) {
            cryptoCurrency = getIntent().getParcelableExtra(KEY_CRYPTO_CURRENCY);
            coinLabelTv.setText(cryptoCurrency.symbol);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        transactionList.clear();
        transactionList.addAll(CryptoTransaction.find(CryptoTransaction.class, "symbol = ?", cryptoCurrency.symbol));

        if (transactionList.size() == 0) {
            showInputDialog(new CryptoTransaction(cryptoCurrency.symbol, 0, CryptoCurrency.getDoublePrice(cryptoCurrency.price)), true);
        }
    }

    private void sortAndUpdateList() {
        Collections.sort(transactionList, CryptoTransaction.getComparator());
        adapter.notifyDataSetChanged();
    }

    private void showInputDialog(CryptoTransaction transaction, boolean newTransaction) {
        TransactionInputDialog dialog = new TransactionInputDialog(this, transaction, newTransaction);
        dialog.setOnSaveTransactionListener(new TransactionInputDialog.OnSaveTransactionListener() {
            @Override
            public void onSaveTransaction(CryptoTransaction transactionChanged, boolean newTransaction) {
                if (newTransaction) {
                    transactionList.add(transactionChanged);
                }
                sortAndUpdateList();
            }
        });
        dialog.show();
    }

    @Override
    public void onItemClick(View view, int position) {
        CryptoTransaction transaction = transactionList.get(position); // Clicked transaction
        showInputDialog(transaction, false);
    }

    @Override
    public void onLongClickListener(View view, int position) {
        final CryptoTransaction transaction = transactionList.get(position); // Clicked transaction
        DialogInterface.OnClickListener yes = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                CryptoTransaction.delete(transaction);
                transactionList.remove(transaction);
                adapter.notifyDataSetChanged();
            }
        };
        Notify.yesCancelDialog(this, "", getResources().getString(R.string.activity_transactions_dialog_delete_check), yes, null);
    }
}
